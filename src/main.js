import Vue from 'vue/dist/vue.js'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import {VueMasonryPlugin} from 'vue-masonry';
import VueFullPage from 'vue-fullpage.js'
import VueRouter from 'vue-router'
import home from "./components/home.vue"
import team from "./components/team.vue"
import services from "./components/services.vue"
import servicesOpen from "./components/services-open.vue"
import portfolio from "./components/portfolio.vue"
import portfolioOpen from "./components/portfolio-open.vue"
import reviews from "./components/reviews.vue"
import work from "./components/work.vue"
import workOpen from "./components/work-open.vue"
import contact from "./components/contact.vue"
import blog from "./components/blog.vue"
import blogOpen from "./components/blog-open.vue"

import 'bootstrap/dist/css/bootstrap.css';

Vue.component('home', home);

Vue.config.productionTip = false

const routes = [
  { path: '/', component: home, name: 'home'},
  { path: '/team', component: team, name: 'team'},
  { path: '/services', component: services, name: 'services'},
  { path: '/services-open', component: servicesOpen, name: 'services-open'},
  { path: '/portfolio', component: portfolio, name: 'portfolio'},
  { path: '/portfolio-open', component: portfolioOpen, name: 'portfolio-open'},
  { path: '/reviews', component: reviews, name: 'reviews'},
  { path: '/work', component: work, name: 'work'},
  { path: '/work-open', component: workOpen, name: 'work-open'},
  { path: '/contact', component: contact, name: 'contact'},
  { path: '/blog', component: blog, name: 'blog'},
  { path: '/blog-open', component: blogOpen, name: 'blog-open'},
]

const router = new VueRouter({
  routes 
})
Vue.use(VueRouter)
Vue.use(VueFullPage);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueMasonryPlugin)

window.$ = require('jquery')

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
